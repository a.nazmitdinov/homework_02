import java.util.Scanner;
/*
Дома дочери Пети опять нужна помощь с математикой! В этот раз ей
нужно проверить, имеет ли предложенное квадратное уравнение решение или
нет.
На вход подаются три числа — коэффициенты квадратного уравнения a, b, c.
Нужно вывести "Решение есть", если оно есть и "Решения нет", если нет.
Ограничения:
-100 < a, b, c < 100
Пример:
    Входные данные      Выходные данные
       1 -95 18           Решение есть
       46 44 3            Решение есть
       34 35 39           Решения нет
       31 -89 4           Решение есть
 */
public class HomeWork5 {
    public static void main(String[] args) {
        Scanner imput = new Scanner(System.in);

        int a = imput.nextInt();
        int b = imput.nextInt();
        int c = imput.nextInt();

        double d = Math.pow(b, 2) - 4 * a * c;

        if (d < 0)
            System.out.println("Решения нет");
        else
            System.out.println("Решение есть");
    }
}
