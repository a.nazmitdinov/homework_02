import java.util.Scanner;
/*
У Марата был взломан пароль. Он решил написать программу,
которая проверяет его пароль на сложность. В интернете он узнал, что пароль
должен отвечать следующим требованиям:
● пароль должен состоять из хотя бы 8 символов;
● в пароле должны быть:
   ○ заглавные буквы
   ○ строчные символы
   ○ числа
   ○ специальные знаки(_*-)
Если пароль прошел проверку, то программа должна вывести в консоль строку пароль
надежный, иначе строку: пароль не прошел проверку
 */

public class HomeWorkAdd1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String s;
        char ch;
        boolean digit = false;
        boolean lowercase = false;
        boolean uppercase = false;
        boolean symbol = false;
        boolean length = false;

        System.out.print("Введите пароль: ");
        s = input.nextLine();


        for (int k=0; k<s.length(); k++){
            ch = s.charAt(k);

            if (Character.isDigit(ch)){
                digit = true;
            }
            if (Character.isLowerCase(ch)){
                lowercase = true;
            }
            if (Character.isUpperCase(ch)){
                uppercase = true;
            }
            if ((ch == '*') || (ch == '_') || (ch == '-')){
                symbol = true;
            }
        }
        if (s.length() >= 8) {
            length = true;
        }

        if (digit == true && lowercase == true && uppercase == true && length == true && symbol == true)
            System.out.println("пароль надежный\n");
        else
            System.out.println("пароль не прошел проверку");
    }
}





