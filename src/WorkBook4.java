public class WorkBook4 {
    /*
    Что будет отображено в консоли следующими фрагментами кода?
    Подсказка: нарисуйте таблицу и перечислите в качестве столбцов
    имена переменных для отслеживания их значений.
     */
    public static void main(String[] args){
        int i = 5;
        while (i >= 1) {
            int num = 1;
            for (int j = 1; j <= i; j++) {
                System.out.print(num + "xxx");
                num *= 2;
            }
            System.out.println();
            i--;
        }
    }
}