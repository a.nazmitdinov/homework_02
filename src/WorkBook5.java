import java.util.Scanner;
/* Напишите программу, которая запрашивает у пользователю
   строку и отображает ее в обратном порядке. Пример запуска:

   Введите строку: ABCD
   Обратная строка равна DCBA
*/

public class WorkBook5 {
    public static void main(String[] args) {
        String s;
        int low, high;

        Scanner input = new Scanner(System.in);
        System.out.println("Введите слово ");
        s = input.nextLine();

        // Инициализировать индекс начала строки
        low = 0;
        // Инициализировать индекс конца строки
        high = s.length() - 1;

        while (low <= high) {
        char tap = s.charAt(high);
            System.out.print(tap);
            high--;
        }
    }
}
