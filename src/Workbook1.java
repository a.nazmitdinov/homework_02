import java.util.Scanner;
/*
Напишите программу, которая запрашивает у пользователя числовые значения,
а выводит количество положительных и количество отрицательных введенных значений.
Подсказка: в качестве значения сигнальной метки используйте 0.
 */
public class Workbook1 {
            public static void main(String[] args) {
            int n;
            int plus, minus;

            plus = 0;
            minus = 0;

            Scanner input = new Scanner(System.in);

            System.out.print("Введите значение n ");
            n = input.nextInt();
                if (n > 0) {
                    System.out.println("Положительное ");
                    plus ++ ;
                    System.out.println("Кол-во положительных чисел: " + plus);
                }
                if (n < 0) {
                    System.out.println("Отрицательное ");
                    minus ++ ;
                    System.out.println("Кол-во отрицательных чисел: " + minus);
                }

            while (n != 0) {
                System.out.println("Введите следующее значение n ");
                n = input.nextInt();
                if (n > 0) {
                    System.out.println("Положительное ");
                    plus ++;
                    System.out.println("Кол-во положительных чисел: " + plus);
                }
                if (n < 0) {
                    System.out.println("Отрицательное ");
                    minus ++ ;
                    System.out.println("Кол-во отрицательных чисел: " + minus);
                }
            }

                System.out.println("Подсчет завершен!");
        }
    }

