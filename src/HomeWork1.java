import java.util.Scanner;
/*
За каждый год работы Петя получает на ревью оценку. На вход
подаются оценки Пети за последние три года (три целых положительных числа).
Если последовательность оценок строго монотонно убывает, то вывести "Петя,
пора трудиться".
В остальных случаях вывести "Петя молодец!"
Ограничения:
0 < a, b, c < 100
  Пример:
            Входные данные            Выходные данные
                10 5 2                Петя, пора трудиться
                4 20 15               Петя молодец!
                5 5 5                 Петя молодец!
 */

public class HomeWork1 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();

        if((a > b && a > c) && (b > c))
            System.out.println("Петя, пора трудиться");
        else
            System.out.println("Петя молодец!");
    }
}
