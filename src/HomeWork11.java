import java.util.Scanner;
/*
На вход подается три целых положительных числа – длины сторон
треугольника. Нужно вывести true, если можно составить треугольник из этих
сторон и false иначе.
Ограничения:
0 < a, b, c < 100
   Пример:
          Входные данные        Выходные данные
             3 2 1                  false
             3 4 5                  true
            2 15 15                 true
            10 2 7                  false
 */
public class HomeWork11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();

        int y = Math.max(Math.max(a, b), c);
        int x = a + b + c;

        if ((x - y) > y)
            System.out.println(true);
        else {
            System.out.println(false);
        }
    }
}
