import java.util.Scanner;
/*
У нас есть почтовая посылка (String mailPackage). Каждая почтовая
посылка проходит через руки проверяющего. Работа проверяющего
заключается в следующем:
● во-первых, посмотреть не пустая ли посылка;
● во-вторых, проверить нет ли в ней камней или запрещенной продукции.
Наличие камней или запрещенной продукции указывается в самой посылке в конце
или в начале. Если в посылке есть камни, то будет написано слово "камни!", если
запрещенная продукция, то будет фраза "запрещенная продукция".
После осмотра посылки проверяющий должен сказать:
● "камни в посылке" – если в посылке есть камни;
● "в посылке запрещенная продукция" – если в посылке есть что-то запрещенное;
● "в посылке камни и запрещенная продукция" – если в посылке находятся камни
и запрещенная продукция;
● "все ок" – если с посылкой все хорошо.
Если посылка пустая, то с посылкой все хорошо.

Напишите программу, которая будет заменять проверяющего.
Входные данные Выходные данные
- мед. чайник. / Вывод: все ок
-              / Вывод: все ок
- камни!       / Вывод: камни в посылке
- запрещенная продукция. вафельница / Вывод: в посылке запрещенная продукция
- камни! ноутбук. запрещенная продукция / Вывод: в посылке камни и запрещенная продукция
 */

public class HomeWorkAdd2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String firstWord, secondWord;
        String text1 = "запрещенная продукция";
        String text2 = "камни";
        boolean index1 = false;
        boolean index2 = false;
        String s = input.nextLine();

        if (s.lastIndexOf(' ') >= 0)
            index1 = true;
        if (s.indexOf('!') >= 0)
            index2 = true;

        if (s.length() == 0)
            System.out.println("все ок");

        if (index1) {
            int number1 = s.lastIndexOf(' ');
            firstWord = s.substring(0, number1 - 1);
            if (firstWord.equals(text1))
                System.out.println("в посылке запрещенная продукция");
            }

        if (index1) {
            int number1 = s.lastIndexOf(' ');
            firstWord = s.substring(0, number1 - 1);
            if (s.lastIndexOf(text1) >= 0 && s.lastIndexOf(text2) >= 0) {
                System.out.println("в посылке " + text2 + " и " + text1);
            } else if (firstWord.compareTo(text1) != 0) {
                System.out.println("все ок");
            }
        }

        if (index2) {
            int number2 = s.indexOf('!');
            firstWord = s.substring(0, number2);
            if (firstWord.equals(text2)) {
                if (s.indexOf(' ') <= 0)
                    System.out.println(firstWord + " в посылке");
            }
            else {
                System.out.println("все ок");
            }
        }

    }
}

