import java.util.Scanner;
/*
Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
Ему хочется уметь разделять строку по первому пробелу. Для этого он может
воспользоваться методами indexOf() и substring().
На вход подается строка. Нужно вывести две строки, полученные из входной
разделением по первому пробелу.
Пример:
   Входные данные            Выходные данные
    Hi great team!               Hi
                                 great team!
    ------------------------------------------
    Hello world!                 Hello
                                 world!
 */
public class HomeWork7 {
    public static void main(String[] args) {
        String s, firstWord, secondWord;

        Scanner input = new Scanner(System.in);
        s = input.nextLine();

        int index = s.indexOf(' ');

        firstWord = s.substring(0,index);
        secondWord = s.substring(index+1);

        System.out.println(firstWord);
        System.out.println(secondWord);
    }
}
