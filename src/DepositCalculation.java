import java.util.Scanner;
import java.time.LocalDate;

public class DepositCalculation {
    static final int MONTH_OF_YEAR = 12; // месяцев в году
    static final int DAYS_YEAR = 365; // дней в году

    public static void main(String[] args) {
        int startMonth; // номер месяца открытия вклада
        int startYear; // год открытия вклада
        int depositTerm; // срок вклада в месяцах
        double rate; // процентная ставка (% годовых)
        double depositAmount; // сумма вклада в рублях
        double accruedInterestPerMonth; // доход по вкладу в текущем месяце
        double accruedInterstTotal =0; // суммарный доход по вкладу

        Scanner input = new Scanner(System.in);
        // Получить номер месяца открытия вклада
        System.out.println("Введите номер месяца открытия вклада");
        startMonth = input.nextInt();

        // Получить год открытия вклада
        System.out.println("Введите год открытия вклада");
        startYear = input.nextInt();

        // Получить срок вклада в месяцах
        System.out.println("Введите срок вклада в месяцах");
        depositTerm = input.nextInt();

        // Получить сумму вклада в рублях
        System.out.println("Введите сумму вклада в рублях");
        depositAmount = input.nextInt();

        // Получить годовую процентную ставку
        System.out.println("Введите годовую процентную ставку");
        rate = input.nextInt();

        int monthsCount = 0; // счетчик месяцев
        int i = startYear;

        while (monthsCount < depositTerm) { // выполняем пока счетчик месяцев меньше срока вклада
            // в цикле будем увеличивать счетчик месяцев на 1
            i ++;  // увеличим год на 1

            for (int j = 1; j <= MONTH_OF_YEAR; j++) { // итерация по месяцам с 1 до 12

                // Если номер месяца меньше или равен номеру месяца открытия вклада
                // то переходим к следующему месяцу, он не попадет в интервал
                if (i == startYear && j <= startMonth) {
                    continue;
                }
                // Если номер текущего месяца равен сроку вклада
                // то расчет заканчиввем и выходим из цикла
                if (monthsCount == depositTerm) {
                    break;
                }
                // Получаем количество дней в текущем месяце
                int lengthOfMonth = LocalDate.of(i, j, 1).lengthOfMonth();

                // Рассчитаем доход по вкладу в текущем месяце
                accruedInterestPerMonth = depositAmount * rate / 100 / DAYS_YEAR * lengthOfMonth;
                accruedInterestPerMonth = Math.round(accruedInterestPerMonth * 100) / 100.0; // округление до копеек

                // Вывести результат начисленных процентов в месяц
                System.out.println("Начислено процентов в " + i + " году " + j + " месяце " + accruedInterestPerMonth);

                // Добавим процент по вкладам текущего месяца к суммарной выплате
                accruedInterstTotal = accruedInterstTotal + accruedInterestPerMonth;

                monthsCount ++; // Увеличим счетчик месяца на один
            }
        }
        // Вывести итоговый результат начисленных процентов
        System.out.println("Всего начислено процентов: " + accruedInterstTotal);
    }
}
