import java.util.Scanner;
/*
Продолжение задачи #7
Раз так легко получается разделять по первому пробелу, Петя решил
немного изменить предыдущую программу и теперь разделять строку по
последнему пробелу.
Пример:
    Входные данные          Выходные данные
     Hi great team!            Hi great
                               team!
    ----------------------------------------
     Hello world!              Hello
                               world!
 */
public class HomeWork8 {
    public static void main(String[] args) {
        String s, firstWord, secondWord;

        Scanner input = new Scanner(System.in);
        s = input.nextLine();

        int index = s.lastIndexOf(' ');

        firstWord = s.substring(0,index);
        secondWord = s.substring(index+1);

            System.out.println(firstWord);
            System.out.println(secondWord);
    }
}

