import java.util.Scanner;
/*
Напишите программу с интерактивным циклом проверки правильности
ввода данных, считывающую пары целых чисел до тех пор, пока не
встретит пару, в которой одно из чисел кратно другому.
 */
public class WorkBook3 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int x, y;

        do {
            System.out.print("Введите числа x и y : ");
            x = input.nextInt();
            y = input.nextInt();

        } while (x % y != 0);
    }
}
