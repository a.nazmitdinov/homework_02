import java.util.Scanner;
/*
Напишите программу, которая проверяет, что при любом x на входе
тригонометрическое тождество (sin^2(x)+ cos^2(x) - 1 == 0) будет
всегда выполняться (то есть будет выводить true при любом x).
Ограничения:
-1000 < x < 1000
  Пример:
         Входные данные              Выходные данные
              90                           true
              0                            true
             -200                          true
 */

public class HomeWork9_1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int x = input.nextInt();

        double y = Math.sin(Math.toRadians(x)) * Math.sin(Math.toRadians(x))
                 + Math.cos(Math.toRadians(x)) * Math.cos(Math.toRadians(x)) - 1.0;

        if (y == 0) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}
